#include "treeop.h"
#include <stdio.h>
#include <string.h>

void nbPost(nbAddr root){
	if (root != NULL){
		nbPost(FirstSon(root));
		printf("%d ", Id(root));
		nbPost(NextBrother(root));
	}
}

void nbPre(nbAddr root){
	if (root != NULL){
			printf("%d ", Id(root));
			nbPre(FirstSon(root));
			nbPre(NextBrother(root));
	}
}

void nbIn(nbAddr root){
	if (root != NULL){
		nbIn(FirstSon(root));
		if (FirstSon(root) == NULL) printf("%d ", Id(root));
		if (Parent(root) != NULL)
			if (FirstSon(Parent(root)) == root)
				printf("%d ", Id(Parent(root)));
		nbIn(NextBrother(root));
	}
}

void nbLevelOrder(nbAddr root,int curLevel, int desLevel){
	if(root != NULL)
	{
		if(curLevel == desLevel)
			printf("%d  ",Id(root));
		nbLevelOrder(FirstSon(root),curLevel+1,desLevel);
		nbLevelOrder(NextBrother(root),curLevel,desLevel);
	}
}

int nbDepth(nbAddr root){
	int y,z;
	if(root == NULL)
		return -1;
	y = nbDepth(FirstSon(root));
	z = nbDepth(NextBrother(root));
	if (y > z)
		return (y+1);
	else
		return (z+1);
}

void nbTrans(nbAddr root, int tab){
	if (root != NULL){
		int i = tab;
		while(i--){
			printf("\t");
		}
		printf("%d-%s",Id(root),Name(root));
		if(nbBrother(FirstSon(root))>0){
			printf(" (%d)",nbBrother(FirstSon(root)));
		}
		puts("");
		if(Price(root) != NULL && Qty(root) != NULL){
			i = tab;
			while(i--){
				printf("\t");
			}
			printf("Rp %d : %d\n",Price(root),Qty(root));
		}
		puts("");
		nbTrans(FirstSon(root),tab+1);
		nbTrans(NextBrother(root),tab);
	}
}

void nbPrint(nbAddr root){
	nbTrans(root,0);
	puts("");
	printf("Size %d\n",nbSize(root));
	printf("Leaf %d\n",nbLeaf(root));
}

void nbPrintSon(nbAddr root){
	if(root != NULL){
		printf("->%s\n",Name(root));
		nbPrintSon(NextBrother(root));
	}
}

nbAddr nbSearchInfo(nbAddr root, nbType src){
	nbAddr nSrc;
	if (root != NULL){
		if (isEqual(Type(nSrc),src))
			return root;
		else{
			nSrc = nbSearchInfo(FirstSon(root),src);
			if (nSrc == NULL)
				return nbSearchInfo(NextBrother(root),src);
			else
				return nSrc;
		}
	}
	return NULL;
}

nbAddr nbSearchId(nbAddr root, int id){
	nbAddr nSrc = NULL;
	if (root != NULL){
		if (Id(root) == id)
			return root;
		else{
			nSrc = nbSearchId(FirstSon(root),id);
			if (nSrc == NULL)
				return nbSearchId(NextBrother(root),id);
			else
				return nSrc;
		}
	}
	return NULL;
}

bool isEqual(nbType n1, nbType n2){
	if(n1.id == n2.id){
		return true;
	}else{
		return false;
	}
}

int nbSize(nbAddr root){
	if(root == NULL){
		return 0;
	}else{
		return (nbSize(FirstSon(root)) + nbSize(NextBrother(root)) + 1);
	}
}

int nbLeaf(nbAddr root){
	if(root == NULL){
		return 0;
	}else if(Price(root) != NULL && Qty(root) != NULL){
		return 	(nbLeaf(NextBrother(root)) + 1);
	}else{
		return (nbLeaf(FirstSon(root)) + nbLeaf(NextBrother(root)));
	}
}

int nbBrother(nbAddr root){
	if(root == NULL){
		return 0;
	}else{
		return nbBrother(NextBrother(root)) + 1;
	}
}

nbAddr chooseSon(nbAddr root, int i){
	if(i>1){
		return chooseSon(NextBrother(root),i-1);
	}else{
		return root;
	}
}

void nbQuery(nbAddr root){
	if(root != NULL){
		if(Price(root) != NULL && Qty(root) != NULL){
			printf("Id : %d\tNama : %s\tKat : %s\tRp %d\tJml = %d\n",Id(root),Name(root),Name(Parent(root)),Price(root),Qty(root));	
		}
		nbQuery(FirstSon(root));
		nbQuery(NextBrother(root));
	}
}

int sumPrice(nbAddr root){
	if(root != NULL){
		int sum = 0;
		if(Price(root) > 0 && Qty(root) > 0){
			sum = (Price(root) * Qty(root));	
		}
		return sumPrice(FirstSon(root)) + sumPrice(NextBrother(root)) + sum;
	}else{
		return 0;
	}
}

void nbSearch(nbTree * root, nbAddr addr, char str[]){
	if(addr != NULL){
		if(Price(addr) != NULL && Qty(addr) != NULL){
			if(strstr(Name(addr),str) != NULL){
				copyNode(&(*root),addr);
			}
		}
		nbSearch(&(*root),FirstSon(addr),str);
		nbSearch(&(*root),NextBrother(addr),str);
	}
}

nbAddr copyNode(nbTree * t, nbAddr n){
	nbType nType;
	nType.id = Id(n);
	strcpy(nType.name,Name(n));
	nType.info.price = Price(n);
	nType.info.qty = Qty(n);
	nbAddr naddr = nbCNode(nType);
	nbInsert(&(*t),Root((*t)),naddr);
	return naddr;
}
