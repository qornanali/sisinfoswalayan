#include "nbtree_d.h"
#include <conio.h>

void nbCreate(nbTree *x){
	Root((*x)) = NULL;
}

nbAddr nbCNode(nbType X){
	nbAddr newNode;
	newNode = (nbAddr) malloc(sizeof(nbTreeNode));
	if (newNode != NULL){
		FirstSon(newNode) = NULL;
		NextBrother(newNode) = NULL;
		Parent(newNode) = NULL;
		Type(newNode) = X;
	}
	return newNode;
}

void nbInsert(nbTree * tRoot, nbAddr parent, nbAddr newson){
	nbAddr temp;
	if (newson !=NULL){ //Jika penciptaan node baru berhasil
		if (parent == NULL) //JIka belum terdapat root
			Root((*tRoot)) = newson;
		else{
			temp = parent;
			if (FirstSon(temp) != NULL){
				temp = FirstSon(temp);
				while(NextBrother(temp) != NULL){
					temp = NextBrother(temp);
				}
				NextBrother(temp) = newson;
			}else{
				FirstSon(temp) = newson;
			}
			Parent(newson) = parent;
		}
	}
}

void nbDelete(nbAddr pDel){
	nbAddr pNext = NextBrother(pDel);
	nbAddr pPrev = FirstSon(Parent(pDel));
	
	while(pPrev != NULL && NextBrother(pPrev) != pDel){
			pPrev = NextBrother(pPrev);
	}
	
	if(FirstSon(Parent(pDel)) == pDel){
		if(pNext != NULL){
			FirstSon(Parent(pDel)) = pNext;	
		}else{
			FirstSon(Parent(pDel)) = NULL;
		}
	}else{
		if(pNext != NULL){
			NextBrother(pPrev) = pNext;	
		}else{
			NextBrother(pPrev) = NULL;
		}
	}
	free(pDel);
}


