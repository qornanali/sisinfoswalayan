/*
File	: nbtree_d.h
Author	: SN
Last Updt : 25-5-2011
*/

#ifndef nbtree_d_H
#define nbtree_d_H

#define Id(N) (N)->type.id
#define Name(N) (N)->type.name
#define Price(N) (N)->type.info.price
#define Qty(N) (N)->type.info.qty
#define Type(N) (N)->type
#define Info(N) (N)->type.info
#define FirstSon(N) (N)->fs
#define NextBrother(N) (N)->nb
#define Parent(N) (N)->p
#define Root(T) (T).root

#include <stdlib.h>
#include <stdbool.h>

typedef struct nbTreeNode *nbAddr; //pointer nbtreenode

typedef struct{
	int price; //price
	int qty; //quantity
} nbInfo;

typedef struct{
	int id; //node id
	char name[60]; //node name
	nbInfo info; //info node
} nbType;

typedef struct nbTreeNode{
	nbAddr fs; //First child
	nbType type; //tipe node
	nbAddr nb; //Next brother
	nbAddr p; //parent
};

struct nbTree{
	nbAddr root;
};

void nbCreate(nbTree *x);  
nbAddr nbCNode(nbType X);
void nbInsert(nbTree *tRoot, nbAddr parent, nbAddr newson);
void nbDelete(nbAddr pDel);

#endif
