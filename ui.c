#include "ui.h"
#include "app.h"

void gotoxy(int x, int y){
	COORD coord;
	coord.X = x; coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);	
}

void slp(int ms){
	Sleep(ms);
}

void cls(){
	system("cls");	
}

void pause(){
	puts("");
	puts("");
	puts("");
	system("PAUSE");
}

void drawshape(int x,int y,int v, int h, int type){
	gotoxy(x,y);
	int garis[6];
	if(type==1){
		garis[0] = duasikukiriatas;
		garis[1] = duasikukananatas;
		garis[2] = duasikukananbawah;
		garis[3] = duasikukiribawah;
		garis[4] = duahor;
		garis[5] = duaver;
	}else{
		garis[0] = sikukiriatas;
		garis[1] = sikukananatas;
		garis[2] = sikukananbawah;
		garis[3] = sikukiribawah;
		garis[4] = hor;
		garis[5] = ver;
	}
	printf("%c",garis[0]);
	int i;
	for(i=0;i<v;i++){
		x++;
		gotoxy(x,y);
		printf("%c",garis[4]);
	} 
	x++;
	printf("%c",garis[1]);
	for(i=0;i<h;i++){
		y++;
		gotoxy(x,y);
		printf("%c",garis[5]);
	} 
	y++;
	gotoxy(x,y);
	printf("%c",garis[2]);
	for(i=0;i<v;i++){
		x--;
		gotoxy(x,y);
		printf("%c",garis[4]);
	} 
	x--;
	gotoxy(x,y);
	printf("%c",garis[3]);
	for(i=0;i<h;i++){
		y--;
		gotoxy(x,y);
		printf("%c",garis[5]);
	}
}

void drawsomechar(int x, int y, int v, int h, char c){
	int i,j;
	for(i=0;i<v;i++){
		for(j=0;j<h;j++){
			gotoxy(x+j,y+i);
			printf("%c",c);
		}
	}
}

int cursor(int x, int y, int xp, int yp, int key1, int key2, int chcursor, int option){
	printf("");
	int select = 1;
	int input;
	gotoxy(x,y);
	printf("%c",chcursor);
	do
	{
		input=getch();
		gotoxy(x,y);printf(" ");
		if(input==key1)
		{

			x+=xp;y+=yp;select++;
			if(select>option)
			{
				x=x-(xp * option);
				y=y-(yp * option);
				select=1;
			}

		}
		if(input==key2)
		{
			x-=xp;y-=yp;select--;
			if(select<1)
			{
				x=x+(xp * option);
				y=y+(yp * option);
				select=option;
			}
		}
		if(input==key_esc){
			select = option;
		}
		gotoxy(x,y);printf("%c",chcursor);
	}
	while(input!=key_enter && input!=key_esc);
	return select;
}

void menuUtama(int x, int y){
	cls();
	drawshape(x,y,28,3,1);
	gotoxy(x+3,y+2);printf("Sistem Informasi Swalayan");
	drawshape(x,y+5,28,7,1);
	gotoxy(x+3,y+7);printf("Menu Admin");
	gotoxy(x+3,y+8);printf("Menu Kasir");
	gotoxy(x+3,y+9);printf("About");
	gotoxy(x+3,y+11);printf("Keluar");
}

void menuAdmin(int x, int y){
	cls();
	drawshape(x,y,28,3,1);
	gotoxy(x+3,y+2);printf("Menu Admin");
	drawshape(x,y+5,28,7,1);
	gotoxy(x+3,y+7);printf("Database");
	gotoxy(x+3,y+8);printf("Simpan Database");
	gotoxy(x+3,y+9);printf("Ganti Password");
	gotoxy(x+3,y+11);printf("Kembali ke menu utama");
}

void menuKasir(int x, int y){
	cls();
	drawshape(x,y,28,3,1);
	gotoxy(x+3,y+2);printf("Menu Kasir");
	drawshape(x,y+5,28,8,1);
	gotoxy(x+3,y+7);printf("Lihat Brosur");
	gotoxy(x+3,y+8);printf("Tambah Barang");
	gotoxy(x+3,y+9);printf("Hapus Barang");
	gotoxy(x+3,y+10);printf("Selesai Belanja");
	gotoxy(x+3,y+12);printf("Kembali ke menu utama");
}

void menuAbout(int x, int y){
	puts("Ali Qornan Jaisyurrahman (151511007)");
	puts("Eka Fitriyah Agustin (151511011)");
	puts("Irmawati Indriani (151511014)");
	printf("\n\n23 Juni 2016");
}

void menuDatabase(int x, int y){
	cls();
	drawshape(x,y,28,3,1);
	gotoxy(x+3,y+2);printf("Menu Database");
	drawshape(x,y+5,28,16,1);
	gotoxy(x+3,y+7);printf("*Barang");
	gotoxy(x+3,y+8);printf("Tambah Barang");
	gotoxy(x+3,y+9);printf("Ubah Barang");
	gotoxy(x+3,y+10);printf("Cari Barang");
	gotoxy(x+3,y+11);printf("Hapus Barang");
	gotoxy(x+3,y+13);printf("*Kategori");
	gotoxy(x+3,y+14);printf("Tambah Kategori");
	gotoxy(x+3,y+15);printf("Ubah Kategori");
	gotoxy(x+3,y+16);printf("Hapus Kategori");
	gotoxy(x+3,y+18);printf("Lihat Tree");
	gotoxy(x+3,y+20);printf("Kembali ke menu admin");
}

void menuPilihKategori(nbAddr root){
	cls();
	if(nbSize(root)-nbLeaf(root)>0){
		printf("Pilih Kategori : \n");
		nbPrintSon(FirstSon(root));
	}else{
		printf("Belum ada data");
	}	
}

void formKategori(int x, int y, int show){
	cls();
	gotoxy(x,y);printf("Id");	
	drawshape(x,y+1,28,1,2);
	if(show == 1){
		gotoxy(x,y+4);printf("Nama Kategori");	
		drawshape(x,y+5,28,1,2);
	}
	gotoxy(x,y+9);printf("Apakah data sudah benar?");
	gotoxy(x,y+10);printf("Ya");
	gotoxy(x,y+11);printf("Tidak");	
}

void formBarang(int x, int y, int show){
	cls();
	gotoxy(x,y);printf("Id");	
	drawshape(x,y+1,28,1,2);
	if(show == 1){
		gotoxy(x,y+4);printf("Nama Barang");	
		drawshape(x,y+5,28,1,2);
		gotoxy(x,y+8);printf("Harga Barang");	
		drawshape(x,y+9,28,1,2);
		gotoxy(x,y+12);printf("Jumlah Barang");	
		drawshape(x,y+13,28,1,2);
	}
	gotoxy(x,y+17);printf("Apakah data sudah benar?");
	gotoxy(x,y+18);printf("Ya");
	gotoxy(x,y+19);printf("Tidak");	
}

void menuTree(nbAddr root){
	cls();
	nbPrint(root);
}

void printnode(nbAddr root, int x, int y){
	gotoxy(x,y);printf("Id : %d\n",Id(root));
	gotoxy(x,y+1);printf("Nama : %s\n",Name(root));
	if(Price(root) != NULL){
		gotoxy(x,y+3);printf("Kategori : %s\n",Name(Parent(root)));
		gotoxy(x,y+4);printf("Harga : Rp %d\n",Price(root));
		gotoxy(x,y+5);printf("Jumlah : %d\n",Qty(root));
	}
}

void menuTambahBelian(int x, int y, int show){
	cls();
	gotoxy(x,y);
//	printf("Ketik nama barang yang ingin dicari dan ditambah ke keranjang");
	printf("Id barang");
	drawshape(x,y+1,50,1,2);
	if(show == 1){
		gotoxy(x,y+4);
		printf("Jumlah barang");
		drawshape(x,y+5,50,1,2);	
	}
//	drawshape(x,y+5,50,40,2);
	gotoxy(x+52,y+2);printf("Apakah data sudah benar?");
	gotoxy(x+52,y+3);printf("Ya");
	gotoxy(x+52,y+4);printf("Tidak");	
}

void menuHapusBelian(int x, int y){
	
}

void menuSelesaiBelanja(int x, int y){
	
}

void menuSearch(int x, int y){
	cls();
	gotoxy(x,y);
	printf("Nama barang");
	drawshape(x,y+1,50,1,2);
	gotoxy(x,y+4);
	printf("Jumlah barang");
	drawshape(x,y+5,60,1,2);	
	drawshape(x,y+8,60,40,2);
	gotoxy(x+65,y+2);printf("Apakah data sudah benar?");
	gotoxy(x+65,y+3);printf("Ya");
	gotoxy(x+65,y+4);printf("Tidak");	
}
