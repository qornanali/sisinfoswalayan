#ifndef ui_H
#define ui_H

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "nbtree_d.h"

#define key_up 72
#define key_down 80
#define key_left 75
#define key_right 77
#define key_enter 13
#define key_esc 27
#define sikukananbawah 217
#define sikukiriatas 218
#define sikukananatas 191
#define sikukiribawah 192
#define duasikukananatas 187
#define duasikukananbawah 188
#define duasikukiriatas 201
#define duasikukiribawah 200
#define duahor 205
#define duaver 186
#define ver 179
#define hor 196
#define leftcursor 17
#define upcursor 30

void menuAdmin(int x, int y);
void menuUtama(int x, int y);
void menuKasir(int x, int y);
void menuAbout(int x, int y);
void menuDatabase(int x, int y);
void menuPilihKategori(nbAddr root);
void menuPilihBarang(nbAddr root);
void menuTree(nbAddr root);
void menuSearch(int x, int y);
void menuTambahBelian(int x, int y, int show);
void menuHapusBelian(int x, int y);
void menuSelesaiBelanja(int x, int y);
void formKategori(int x, int y, int show);
void formBarang(int x, int y, int show);
void showInventory();
void printnode(nbAddr root, int x, int y);
void slp(int ms); 
void gotoxy(int x, int y);
void cls();
void pause();
void drawshape(int x, int y, int v, int h, int type); 
void drawsomechar(int x, int y, int v, int h, char c); 
int cursor(int x, int y, int xp, int yp, int key1, int key2, int chcursor, int option); 

#endif
