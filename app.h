#ifndef app_H
#define app_H


#include <conio.h>
#include "ui.h"
#include "treeop.h"
#include "nbtree_d.h"

void intialize();
void prepareTree(nbTree * tree);
void loadMenuUtama();
void loadMenuAdmin();
void loadMenuKasir();
void loadMenuAbout();
void loadMenuDB();
void manualinsert();
void saveDB();

void doTambahKategori();
void doTambahBarang();
void doHapusKategori();
void doHapusBarang();
void doCariBarang();
void doUbahBarang();
void doUbahKategori();
void doTambahBelian();
void doHapusBelian();
void doSelesaiBelanja();

void loadKatalog();

#endif
