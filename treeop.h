#ifndef treeop_H
#define treeop_H

#include "nbtree_d.h"


void nbPost(nbAddr root); 
void nbPre(nbAddr root);
void nbIn(nbAddr root); 
void nbLevelOrder(nbAddr root,int curLevel, int desLevel);
void nbPrint(nbAddr root);
void nbTrans(nbAddr root, int tab);
void nbPrintSon(nbAddr root);
void nbQuery(nbAddr root);
void nbSearch(nbTree * root, nbAddr addr, char str[]);

bool isEqual(nbType n1, nbType n2);

nbAddr nbSearchInfo(nbAddr root, nbType src);
nbAddr nbSearchId(nbAddr root, int id);
nbAddr chooseSon(nbAddr root, int i);
nbAddr copyNode(nbTree * t, nbAddr n);

int nbDepth(nbAddr root);
int nbSize(nbAddr root);
int nbLeaf(nbAddr root);
int nbBrother(nbAddr root);
int sumPrice(nbAddr root);


#endif
