/*memuat method2 si aplikasi*/
#include "app.h"

nbTree Tree,Keranjang,Query;

void intialize(){
	prepareTree(&Tree);
	prepareTree(&Keranjang);
	prepareTree(&Query);
}

void prepareTree(nbTree * tree){
	nbCreate(&(*tree));
	nbType nType;
	nbInfo nInfo;
	nInfo.price = NULL;
	nInfo.qty = NULL;
	nType.id = 0;
	strcpy(nType.name,"Root");
	nbAddr root1 = nbCNode(nType);
	nbInsert(&(*tree),Root((*tree)),root1);
}

void manualinsert(){
	
}

void loadMenuUtama(){
	menuutama :
	menuUtama(1,1);
	int menu = cursor(16,8,0,1,key_down,key_up,leftcursor,5); 
	switch(menu){
		case 1 :
			loadMenuAdmin();
			break;
		case 2 :
			loadMenuKasir();
			break;
		case 3 :
			loadMenuAbout();
			break;
	}
	if(menu>=1 && menu<=4){
		pause();
		goto menuutama;
	}else{
		system("EXIT");
	}
}

void loadMenuAdmin(){
	menuadmin :
	menuAdmin(1,1);
	int menu = cursor(26,8,0,1,key_down,key_up,leftcursor,5); 
	switch(menu){
		case 1 :
			loadMenuDB();
			break;
		case 2 :
			break;
		case 3 :
			break;
	}
	if(menu>=1 && menu<=4){
		pause();
		goto menuadmin;
	}
}

void loadMenuKasir(){
	
	prepareTree(&Keranjang);
	prepareTree(&Query);
	
	menukasir :
	menuKasir(1,1);
	int menu = cursor(26,8,0,1,key_down,key_up,leftcursor,6); 
	switch(menu){
		case 1 :
			cls();
			if(nbLeaf(Root(Tree))>0){
				nbQuery(Root(Tree));	
			}else{
				puts("Belum ada barang di database");
			}
			break;
		case 2 :
			doTambahBelian();
			break;
		case 3 :
			doHapusBelian();
			break;
		case 4 :
			doSelesaiBelanja();
			break;
	}
	if(menu>=1 && menu<=5){
		pause();
		goto menukasir;
	}
}

void loadMenuAbout(){
	cls();
	menuAbout(1,1);
}

void loadMenuDB(){
	menuDB :
	menuDatabase(1,1);
	int menu = cursor(26,8,0,1,key_down,key_up,leftcursor,14); 
	switch(menu){
		case 2 :
			doTambahBarang();
			break;
		case 3 :
			doUbahBarang();
			break;
		case 4 :
			doCariBarang();
			break;
		case 5 :
			doHapusBarang();
			break;
		case 8 :
			doTambahKategori();
			break;
		case 9 :
			doUbahKategori();
			break;
		case 10 :
			doHapusKategori();
			break;
		case 12 :
			menuTree(Root(Tree));
			break;
	}
	if(menu>=1 && menu<=13){
		pause();
		goto menuDB;
	}
}

void doTambahKategori(){
	nbType sonType;
	nbInfo sonInfo;
	int pilihan = 1;
	do{
		do{
			formKategori(1,1,1);
			gotoxy(2,3);
			scanf("%d",&sonType.id);
		}while((nbSearchId(Root(Tree),sonType.id)) != NULL);
		gotoxy(2,7);
		scanf("%s",&sonType.name);
		pilihan = cursor(7,11,0,1,key_down,key_up,leftcursor,2); 
	}while(pilihan == 2);
	sonInfo.price = NULL;
	sonInfo.qty = NULL;
	sonType.info = sonInfo;
	nbAddr son = nbCNode(sonType);
	nbInsert(&Tree,Root(Tree),son);
	cls();
	printf("Data sudah berhasil dimasukkan");
	printnode(son,1,2);
}

void doTambahBarang(){
	nbAddr P = Root(Tree);
	if(nbBrother(FirstSon(P))>0){
		int pilihan = 1;
		do{
			menuPilihKategori(P);
			pilihan = cursor(10,1,0,1,key_down,key_up,leftcursor,nbBrother(FirstSon(P))); 
			P = chooseSon(FirstSon(P),pilihan);
		}while(P == NULL);
		nbType sonType;
		nbInfo sonInfo;
		pilihan = 1;
		do{
			do{
				formBarang(1,1,1);
				gotoxy(2,3);
				scanf("%d",&sonType.id);
			}while((nbSearchId(Root(Tree),sonType.id)) != NULL);
			gotoxy(2,7);
			scanf("%s",&sonType.name);
			do{
				gotoxy(2,11);
				scanf("%d",&sonInfo.price);	
			}while(sonInfo.price < 1);
			gotoxy(2,15);
			scanf("%d",&sonInfo.qty);
			pilihan = cursor(7,19,0,1,key_down,key_up,leftcursor,2); 
		}while(pilihan == 2);
		sonType.info = sonInfo;
		nbAddr son = nbCNode(sonType);
		nbInsert(&Tree,P,son);
		cls();
		printf("Data sudah berhasil dimasukkan");
		printnode(son,1,2);	
	}
}
void doHapusKategori(){
	int idKategori;
	nbAddr pDel = NULL;
	int pilihan = 1;
	do{
		formKategori(1,1,2);
		gotoxy(2,3);
		scanf("%d",&idKategori);
		pilihan = cursor(8,11,0,1,key_down,key_up,leftcursor,2);
		pDel = nbSearchId(Root(Tree),idKategori);
	}while(pilihan == 2 || pDel == NULL || (Price(pDel) != NULL && Qty(pDel) != NULL));
	nbDelete(pDel);
	cls();
	printf("Data sudah berhasil dihapus");
}
void doHapusBarang(){
	int idBarang;
	nbAddr pDel = NULL;
	int pilihan = 1;
	do{
		formBarang(1,1,2);
		gotoxy(2,3);
		scanf("%d",&idBarang);
		pilihan = cursor(7,19,0,1,key_down,key_up,leftcursor,2);
		pDel = nbSearchId(Root(Tree),idBarang);
	}while(pilihan == 2 || pDel == NULL || (Price(pDel) == NULL && Qty(pDel) == NULL));
	nbDelete(pDel);
	cls();
	printf("Data sudah berhasil dihapus");
}
void doCariBarang(){
	int idBarang;
	nbAddr pSearch = NULL;
	do{
		formBarang(1,1,2);
		gotoxy(2,3);
		scanf("%d",&idBarang);
		pSearch = nbSearchId(Root(Tree),idBarang);
	}while(pSearch == NULL || (Price(pSearch) == NULL && Qty(pSearch) == NULL));
	cls();
	printnode(pSearch,1,1);	
}
void doUbahBarang(){
	int idBarang;
	int pilihan;
	nbAddr pSearch = NULL;
	do{
		do{
			formBarang(1,1,1);
			gotoxy(2,3);
			scanf("%d",&idBarang);
			pSearch = nbSearchId(Root(Tree),idBarang);
		}while(pSearch == NULL || (Price(pSearch) == NULL && Qty(pSearch) == NULL));
		gotoxy(2,7);
		scanf("%s",&(Name(pSearch)));
		do{
			gotoxy(2,11);
			scanf("%d",&(Price(pSearch)));	
		}while(Price(pSearch) < 1);
		gotoxy(2,15);
		scanf("%d",&(Qty(pSearch)));
		pilihan = cursor(7,19,0,1,key_down,key_up,leftcursor,2); 
	}while(pilihan == 2);
	cls();
	printf("Data sudah berhasil diubah");
	printnode(pSearch,1,2);	
}
void doUbahKategori(){
	int idKategori;
	nbAddr pSearch = NULL;
	int pilihan = 1;
	do{
		do{
			formKategori(1,1,1);
			gotoxy(2,3);
			scanf("%d",&(idKategori));
			pSearch = nbSearchId(Root(Tree),idKategori);
		}while(pSearch == NULL || (Price(pSearch) != NULL && Qty(pSearch) != NULL));
		gotoxy(2,7);
		scanf("%s",&(Name(pSearch)));
		pilihan = cursor(7,19,0,1,key_down,key_up,leftcursor,2); 
	}while(pilihan == 2);
	cls();
	printf("Data sudah berhasil diubah");
	printnode(pSearch,1,2);
}

void doTambahBelian(){
	if(nbLeaf(Root(Tree))>0){
		int pilihan;
		char str[255];
		int id,jml;
		nbAddr pSearch = NULL;
		nbAddr pQuery;
//		do{
//			do{
//				menuTambahBelian(0,0,1);
//				gotoxy(1,2);
//				scanf("%d",&id);
//				pSearch = nbSearchId(Root(Tree),id);
//			}while(pSearch==NULL || (Price(pSearch) == NULL && Qty(pSearch) == NULL));
//			printnode(pSearch,0,10);
//			do{
//				gotoxy(1,6);
//				scanf("%d",&jml);	
//			}while(jml<1 || jml>Qty(pSearch));
//			pilihan = cursor(60,3,0,1,key_down,key_up,leftcursor,2); 
//		}while(pilihan == 2);
//		pQuery = copyNode(&Query,pSearch);
//		Qty(pQuery) = jml;
		do{
			do{
				menuSearch(0,0);
				gotoxy(1,2);
				scanf("%s",&str);
				gotoxy(0,9);
				prepareTree(&Keranjang);
				nbSearch(&Keranjang,Root(Tree),str);
				nbQuery(FirstSon(Root(Keranjang)));	
				pilihan = cursor(70,9,0,1,key_down,key_up,leftcursor,nbLeaf(Root(Keranjang))); 
				pQuery = chooseSon(FirstSon(Root(Keranjang)),pilihan);
				pSearch = nbSearchId(Root(Tree),Id(pQuery));
				do{
					gotoxy(1,6);
					scanf("%d",&jml);	
				}while(jml<1 || jml>Qty(pSearch));
			}while(pSearch == NULL);
			pilihan = cursor(70,3,0,1,key_down,key_up,leftcursor,2); 
		}while(pilihan == 2);
		cls();
		pQuery = copyNode(&Query,pSearch);
		Qty(pQuery) = jml;
		printf("barang berhasil ditambahkan ke keranjang");
		printnode(pSearch,1,2);		
	}
}

void doHapusBelian(){
	if(nbBrother(FirstSon(Root(Query)))>0){
		int pilihan;
		nbAddr pChoose = NULL;
		do{
			cls();
			nbQuery(FirstSon(Root(Query)));
			pilihan = cursor(40,0,0,1,key_down,key_up,leftcursor,nbBrother(FirstSon(Root(Query))));
			pChoose = chooseSon(FirstSon(Root(Query)),pilihan);
			gotoxy(52,2);printf("Apakah data sudah benar?");
			gotoxy(52,3);printf("Ya");
			gotoxy(52,4);printf("Tidak");
			pilihan = cursor(65,3,0,1,key_down,key_up,leftcursor,2); 
		}while(pilihan == 2);
		nbDelete(pChoose);
		cls();
		printf("barang berhasil dihapuskan dari keranjang");
	}
}

void doSelesaiBelanja(){
	cls();
	int pilihan;
	nbAddr pSearch;
	nbAddr P = NULL;
	nbAddr pTemp = NULL;
	if(nbBrother(Root(Query))>0){
		nbQuery(FirstSon(Root(Query)));	
		printf("\nTotal pembelian : Rp %d",sumPrice(FirstSon(Root(Query))));
		gotoxy(52,2);printf("Apakah data sudah benar?");
		gotoxy(52,3);printf("Ya");
		gotoxy(52,4);printf("Tidak");
		pilihan = cursor(65,3,0,1,key_down,key_up,leftcursor,2);
		if(pilihan == 1){
			P = FirstSon(Root(Query));
			while(P != NULL){
				pSearch = nbSearchId(Root(Tree),Id(P));
				Qty(pSearch) = 	Qty(pSearch) - Qty(P);
				pTemp = P;
				P = NextBrother(P);
				nbDelete(pTemp);
				if(Qty(pSearch) <= 0){
					nbDelete(pSearch);
				}
			}
		} 
	}else{
		printf("tidak ada barang apapun di keranjang");
	}
}
